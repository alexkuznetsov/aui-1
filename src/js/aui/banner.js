'use strict';

import $ from './jquery';
import { recomputeStyle } from './internal/animation';
import amdify from './internal/amdify';
import globalize from './internal/globalize';
import template from './template';

var ID_BANNER_CONTAINER = 'header';

function banner (options) {
    var $banner = renderBannerElement(options);

    pruneBannerContainer();
    insertBanner($banner);

    return $banner[0];
}

function renderBannerElement(options) {
    var html =
    '<div class="aui-banner aui-banner-{type}" role="banner">' +
    '{body}' +
    '</div>';

    var $banner = $(template(html).fill({
        'type': 'error',
        'body:html': options.body || ''
    }).toString());

    return $banner;
}

function pruneBannerContainer() {
    var $container = findContainer();
    var $allBanners = $container.find('.aui-banner');

    $allBanners.get().forEach(function (banner) {
        var isBannerAriaHidden = banner.getAttribute('aria-hidden') === 'true';
        if (isBannerAriaHidden) {
            $(banner).remove();
        }
    });
}

function findContainer() {
    return $('#' + ID_BANNER_CONTAINER);
}

function insertBanner($banner) {
    var $bannerContainer = findContainer();
    if (!$bannerContainer.length) {
        throw new Error('You must implement the application header');
    }

    $banner.prependTo($bannerContainer);
    recomputeStyle($banner);
    $banner.attr('aria-hidden', 'false');
}

amdify('aui/banner', banner);
globalize('banner', banner);
export default banner;
