'use strict';

import globalize from './internal/globalize';
import indexOf from './index-of';

/**
 * Looks for an element inside the array.
 *
 * @param {Array} array The array being searched.
 * @param {Array} item The current item.
 *
 * @return {Boolean}
 */
function contains (array, item) {
    return indexOf(array, item) > -1;
}

globalize('contains', contains);

export default contains;
