package it.com.atlassian.aui.javascript.integrationTests;

import it.com.atlassian.aui.javascript.AbstractAuiIntegrationTest;
import it.com.atlassian.aui.javascript.pages.InfrastructureTestPage;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * @since 4.0
 */
public class AUIInfrastructureTest extends AbstractAuiIntegrationTest
{
    InfrastructureTestPage infrastructurePage;

    @Before
    public void setup()
    {
        infrastructurePage = product.visit(InfrastructureTestPage.class);
    }

    @Test
    public void testJqueryIsLoaded()
    {
        assertTrue("jQuery is expected to be loaded in the page", infrastructurePage.jQueryIsLoaded());
    }

    @Test
    public void testOSPluginIsLoaded()
    {
        assertTrue("The AJS.$.os plugin is expeceted to be loaded.", infrastructurePage.osIsLoaded());
    }

    @Test
    public void testUnderscoreIsNotLoaded()
    {
        assertFalse("Underscore is not expeceted to be loaded by default", infrastructurePage.underscoreIsLoaded());
    }
}
