package it.com.atlassian.aui.javascript.integrationTests.experimental;

import com.atlassian.pageobjects.elements.PageElementFinder;
import com.atlassian.pageobjects.elements.WebDriverElement;
import it.com.atlassian.aui.javascript.AbstractAuiIntegrationTest;
import it.com.atlassian.aui.javascript.pages.ProgressIndicatorTestPage;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class AUIProgressIndicatorTest extends AbstractAuiIntegrationTest
{

    private ProgressIndicatorTestPage progressBarTestPage;
    private PageElementFinder elementFinder;

    @Before
    public void setUp()
    {
        progressBarTestPage = product.getTestedProduct().visit(ProgressIndicatorTestPage.class);
        elementFinder = progressBarTestPage.getElementFinder();
    }

    @Test
    public void testSetIndeterminate()
    {
        WebDriverElement toggleButton = (WebDriverElement) elementFinder.find(By.id("toggle-progress-button"));
        WebDriverElement progressBar = (WebDriverElement) elementFinder.find(By.id("test-toggle-progress-bar"));
        toggleButton.click();
        assertFalse(progressBar.hasAttribute("data-value", ""));
    }

    @Test
    public void testUpdate()
    {
        WebDriverElement progressBar = (WebDriverElement) elementFinder.find(By.cssSelector("#test-static-progress-bar .aui-progress-indicator-value"));
        WebDriverElement progressBarContainer = (WebDriverElement) elementFinder.find(By.id("test-static-progress-bar"));

        assertTrue(progressBar.getAttribute("data-progress") == null);

        progressBar.javascript().execute("AJS.progressBars.update('#test-static-progress-bar', 0.5)");

        assertTrue(progressBar.asWebElement().getSize().getWidth() > 0);
        assertTrue(progressBarContainer.getAttribute("data-value").equals("0.5"));
    }
}
