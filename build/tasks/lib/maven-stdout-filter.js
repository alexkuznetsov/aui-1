module.exports = function (verbose) {

    return function(data) {
        data = data.toString().trim();
        // Only output debug data in verbose mode.
        if (data.startsWith('[DEBUG]')) {
            if (verbose) {
                return console.log(data);
            }
        }
        if (data.startsWith('[WARNING]')) {
            return console.warn(data);
        }
        if (data.startsWith('[ERROR]')) {
            return console.error(data);
        }
        return console.log(data);
    };
};
