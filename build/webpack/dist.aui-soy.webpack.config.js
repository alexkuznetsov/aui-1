const path = require('path');
const merge = require('webpack-merge');
const { librarySkeleton } = require('./webpack.skeleton');

module.exports = merge([
    librarySkeleton,

    {
        entry: {
            'aui-soy': path.resolve('src', 'entry', 'aui-soy.js'),
            'aui-soy.min': path.resolve('src', 'entry', 'aui-soy.js')
        }
    },
]);
