var gulp = require('gulp');
var gat = require('gulp-auto-task');
var { buildWithWebpack } = require('../webpack');
var auiDocs = require('../../webpack/docs.webpack.config');

module.exports = gulp.series(
    gat.load('docs/metalsmith'),
    buildWithWebpack(auiDocs)
);
