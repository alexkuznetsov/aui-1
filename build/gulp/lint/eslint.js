'use strict';

var assign = require('object-assign');
var gat = require('gulp-auto-task');
var gulp = require('gulp');
var eslint = require('gulp-eslint');

module.exports = function lintWithEslint () {
    var opts = assign({
        files: [
            'gulpfile.js',
            'src/js/**/*.js',
            'tests/**/*.js'
        ]
    }, gat.opts());
    return gulp.src(opts.files)
        .pipe(eslint())
        .pipe(eslint.format());
};
