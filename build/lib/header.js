const header = require('gulp-header');
const banner = require('./banner');

module.exports = () => header(banner);
