'use strict';

const gulpOpts = require('gulp-auto-task').opts();
const gulpTap = require('gulp-tap');
const pkg = require('../../package.json');

module.exports = function () {
    return gulpTap(function (file) {
        const opts = Object.assign({}, gulpOpts);
        const version = opts.docsVersion || pkg.version;
        var code = file.contents.toString().replace(/AUI_VERSION/g, `'${pkg.version}'`);
        file.contents = new Buffer(code);
    });
};
