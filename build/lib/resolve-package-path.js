'use strict';

var path = require('path');

/**
 * Uses require.resolve() to resolve the package corresponding to `packageName`
 * and join its resolved root directory to `path` if provided, otherwise just
 * return the resolved root directory of the package.
 *
 * @param {!String} packageName package to resolve relative to.
 * @param {!String} [relativePath] optional path to join to the package's root directory.
 * @return {String} The resolved path to the package (+ optional relative path)
 */
module.exports = function (packageName, relativePath) {
    var packageJsonPath = require.resolve(path.join(packageName, 'package.json'));
    var basePath = path.dirname(packageJsonPath);
    return relativePath === undefined ? basePath : path.join(basePath, relativePath);
};
