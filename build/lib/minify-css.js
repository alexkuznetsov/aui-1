var gulpCssnano = require('gulp-cssnano');

module.exports = function () {
  return gulpCssnano({
    // Added to avoid erroneous shortening of border-x properties in to border.
    // see: https://github.com/ben-eb/cssnano/issues/116
    // note: this is "fixed" in cssnano 3.0.
    mergeLonghand: false,
    // Added to avoid keyframe animations across multiple compilations being given the same name.
    // see: https://ecosystem.atlassian.net/browse/AUI-4749
    reduceIdents: false,
  });
};
