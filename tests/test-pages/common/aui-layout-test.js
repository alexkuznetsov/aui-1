(function(){

    function setClassToggles(options) {
        var theTarget = AJS.$(options.target);
        var classes = (theTarget.attr('class') || '').split(/\s+/);
        var theClass;
        var triggerWrap = AJS.$(options.triggerWrap);
        var triggers = [];
        var trigger;

        AJS.$.each(classes, function(i) {
            trigger = AJS.$("<button></button>").attr("id",classes[i]).text(classes[i]);
            triggerWrap.append(trigger);
            triggers.push(trigger);
        });

        function updateMessage(trigger,target,value) {
            if (target.hasClass(value)) {
                trigger.text("Turn " + value + " off");
            } else {
                trigger.text("Turn " + value + " on");
            }
        };

        AJS.$.each(triggers, function() {
            // set up button text
            updateMessage(AJS.$(this), theTarget, AJS.$(this).attr("id"));

            // toggle class and text on click
            AJS.$(this).click(function() {
                theClass = AJS.$(this).attr("id");
                theTarget.toggleClass(theClass);
                updateMessage(AJS.$(this),theTarget,theClass);
            })
        });
    };

    AJS.$(document).ready(function() {
        setClassToggles({ "triggerWrap": "#toggles", "target": "body" });
    });

}());
