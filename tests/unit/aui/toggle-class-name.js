'use strict';

import toggleClassName from '../../../src/js/aui/toggle-class-name';

describe('aui/toggle-class-name', function () {
    it('globals', function () {
        expect(AJS.toggleClassName.toString()).to.equal(toggleClassName.toString());
    });
});
