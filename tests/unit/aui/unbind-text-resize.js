'use strict';

import unbindTextResize from '../../../src/js/aui/unbind-text-resize';

describe('aui/unbind-text-resize', function () {
    it('globals', function () {
        expect(AJS.unbindTextResize.toString()).to.equal(unbindTextResize.toString());
    });
});
