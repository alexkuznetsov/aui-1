import $ from '../../src/js/aui/jquery';
import _ from '../../src/js/aui/underscore';
import { undim } from '../../src/js/aui/blanket';
import CustomEvent from '../../src/js/aui/polyfills/custom-event';
import format from '../../src/js/aui/format';
import keyCode from '../../src/js/aui/key-code';
import layerManagerGlobal from '../../src/js/aui/layer-manager-global';

/* global chai, sinon */

function dispatch (event, target, data) {
    var orig = target;
    target = (typeof target === 'string') ? $(target) : target;
    target = (target instanceof $) ? target[0] : target;

    if (typeof event === 'string') {
        event = new CustomEvent(event, {
            bubbles: true,
            cancelable: true,
            detail: data
        });
    }

    if (!target || typeof target.dispatchEvent !== 'function') {
        var msg = format('The object provided to dispatch events to did not resolve to a DOM element: was {0}', String(orig));
        var err = new Error(msg);
        err.target = target;
        throw err;
    }

    target.dispatchEvent(event);
}

function ensureHtmlElement (el) {
    if (typeof el === 'string') {
        var div = document.createElement('div');
        div.innerHTML = el;
        return div.children.item(0);
    }

    if (el instanceof $) {
        return el.get(0);
    }

    return el;
}

function getLayers () {
    return $('.aui-layer');
}

function createFixtureItems (fixtureItems, removeOldFixtures, fixtureElement) {
    fixtureElement = fixtureElement || document.getElementById('test-fixture');

    if (removeOldFixtures || removeOldFixtures === undefined) {
        fixtureElement.innerHTML = '';
    }

    if (fixtureItems) {
        for (var name in fixtureItems) {
            if (fixtureItems.hasOwnProperty(name)) {
                fixtureItems[name] = ensureHtmlElement(fixtureItems[name]);
                fixtureElement.appendChild(fixtureItems[name]);
            }
        }
    }

    return fixtureItems;
}

function removeLayers () {
    var $layer;

    while ($layer = layerManagerGlobal.getTopLayer()) {
        layerManagerGlobal.popUntil($layer);
        $layer.remove();
    }

    getLayers().remove();
    undim();
    $('.aui-blanket').remove();
}

function click (element) {
    dispatch('click', element);
}

function mousedown (element) {
    dispatch('mousedown', element);
}

function hover (element) {
    ['mouseenter','mouseover','mousemove'].forEach(function (name) {
        dispatch(name, element);
    });
}

function pressKey (key, modifiers, onElement) {
    let e = new CustomEvent('keydown', {
        bubbles: true,
        cancelable: true
    });
    let keyPress = new CustomEvent('keypress', {
        bubbles: true,
        cancelable: true
    });

    modifiers = modifiers || {};

    if (typeof key === 'string') {
        let ucKey = key.toUpperCase();
        if (typeof keyCode[ucKey] === 'number') {
            key = keyCode[ucKey];
        } else {
            key = key.charCodeAt(0);
        }
    }

    [keyPress, e].forEach(event => {
        event.keyCode = key;
        event.ctrlKey = !!modifiers.control;
        event.shiftKey = !!modifiers.shift;
        event.altKey = !!modifiers.alt;
        event.metaKey = !!modifiers.meta;
    });

    dispatch(keyPress, onElement || document.activeElement);
    dispatch(e, onElement || document.activeElement);
}

function fakeTypingOut (stringInput, onElement) {
    onElement = onElement || document.activeElement;

    String(stringInput).split('').forEach(function(char) {
        pressKey(char, onElement);
        onElement.value += char;

        if (setTimeout.clock) {
            setTimeout.clock.tick(1);
        }
    });
}

function fakeBackspace (onElement, numTimes) {
    onElement = onElement || document.activeElement;
    numTimes = typeof numTimes === 'number' ? numTimes : 1;

    _(numTimes).times(function () {
        pressKey(onElement);
        onElement.value = onElement.value.substring(0, onElement.value.length - 1);

        if (setTimeout.clock) {
            setTimeout.clock.tick(1);
        }
    });
}

function fakeClear (withKey, onElement) {
    onElement = onElement || document.activeElement;
    fakeBackspace(onElement, onElement.value.length);
    pressKey(withKey || 'escape');
}

var realTimeout = window.setTimeout;
function afterMutations (callback, delay) {
    realTimeout(callback, typeof delay === 'number' ? delay : 1);
}

function respondWithJson (server, pattern, json) {
    if (arguments.length === 2) {
        json = pattern;
        pattern = /.*/;
    }

    server.respondWith(pattern, [200, 'application/json', JSON.stringify(json)]);
}

function focus($element){
    var element = $($element)[0];
    element.focus();
    element.dispatchEvent(new CustomEvent('focus'));
}

function blur($element) {
    var element = $($element)[0];
    element.blur();
    element.dispatchEvent(new CustomEvent('blur'));
}

// Chai extensions
// ---------------
chai.use(function (chai, utils) {
    utils.addProperty(chai.Assertion.prototype, 'visible', function () {
        var $el = $(this._obj);
        this.assert(
            $el.is(':visible') === true,
            'expected "#{this}" to be visible',
            'expected "#{this}" to be hidden'
        );
    });
});

export default {
    afterMutations: afterMutations,
    click: click,
    dispatch: dispatch,
    ensureHtmlElement: ensureHtmlElement,
    mousedown: mousedown,
    fakeBackspace: fakeBackspace,
    fakeClear: fakeClear,
    fakeTypingOut: fakeTypingOut,
    fixtures: createFixtureItems,
    hover: hover,
    pressKey: pressKey,
    removeLayers: removeLayers,
    respondWithJson: respondWithJson,
    focus: focus,
    blur: blur
};
